
CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Installation
 * Configuration
 * Usage


INTRODUCTION
------------

Current Maintainer: Jennifer Trelewicz <http://drupal.org/user/1601538> <http://tgpo.ru>

Telegram health monitor provides hooks for use by custom modules for performing cron-based
healthchecks. Errors in the checks are reported through a Telegram bot to a Telegram channel.

A heartbeat check is provided that sends a timestamp on each cron run. This can be disabled
in the configuration page.

Requires telegram_bot.

INSTALLATION
------------

Copy the 'telegram_health_monitor' module directory in to your Drupal
sites/all/modules directory and enable as you would any other module.

CONFIGURATION
-------------

Configure at admin/config/telegram_health_monitor:

1. Required: indicate the channel (chat_id) to which the bot should report. The channel
must be created in Telegram. Do not include the leading '@'.

Public channels have a name given at the time of setup.

Private channels do not have a name, but if you open the channel on the Telegram web
interface, you will see a URL in the format 
https://web.telegram.org/#/im?p=cdigits_digits
Take the digits after the c and before the underscore, prepend -100 and this is the
channel ID.

2. Optional: set start and stop working hours for the built-in heartbeat report

USAGE
-----

This module is intended to be used by other modules, although the heartbeat can be
used to verify the receipt of messages on your channel.
